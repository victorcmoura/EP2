
package Controller;
import java.io.*;
import java.util.*;


public class Arquivo {
    private String caminho;
    private int nlinhas;
    
    public Arquivo(String c){
        caminho = c;
        
    }
    
    public String leitor(int nlinha) throws Exception{
        try (BufferedReader bufferLeitura = new BufferedReader(new FileReader(caminho))) {
            String linha = " ";
            for(int x=0; x<nlinha; x++){linha = bufferLeitura.readLine();}
            return linha;
        }
        
    }
    
    public void escritor(String linha) throws Exception{
        try (BufferedWriter bufferEscrita = new BufferedWriter(new FileWriter(caminho))) {
            bufferEscrita.append(linha+"\n");
        }
        
    }
}
