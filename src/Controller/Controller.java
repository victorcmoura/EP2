package Controller; 
import View.*;
import Model.*;
import java.util.*;




public class Controller {
    
    LoginJFrame telaLogin;
    mainJFrame telaMain;
    Estoque estoqueRestaurante;
    String caminhoLogin = "login.bin";
    String caminhoSenha = "senha.bin";
    String caminhoEstoque_nome = "estoquenome.bin";
    String caminhoEstoque_preco = "estoquepreco.bin";
    String caminhoEstoque_categoria = "estoquecategoria.bin";
    String caminhoEstoque_id = "estoqueid.bin";
    String caminhoEstoque_qtde = "estoqueqtde.bin";
    
    Set<Pedido> ListaDePedidos;
    String[] nameListaDePedidos;
    
    Arquivo bancoDeLogin;
    Arquivo bancoDeSenha;
    Arquivo bancoDeEstoque_nome;
    Arquivo bancoDeEstoque_preco;
    Arquivo bancoDeEstoque_categoria;
    Arquivo bancoDeEstoque_id;
    Arquivo bancoDeEstoque_qtde;
            
    
    public Controller(){
        ///inicialização dos arquivos de banco de dados
        bancoDeLogin = new Arquivo(caminhoLogin);
        bancoDeSenha = new Arquivo(caminhoSenha);
        bancoDeEstoque_nome = new Arquivo(caminhoEstoque_nome);
        bancoDeEstoque_preco = new Arquivo(caminhoEstoque_preco);
        bancoDeEstoque_categoria = new Arquivo(caminhoEstoque_categoria);
        bancoDeEstoque_id = new Arquivo(caminhoEstoque_id);
        bancoDeEstoque_qtde = new Arquivo(caminhoEstoque_qtde);
        ///inicialização do estoque
        estoqueRestaurante = new Estoque(bancoDeEstoque_nome, bancoDeEstoque_preco, bancoDeEstoque_id, bancoDeEstoque_categoria, bancoDeEstoque_qtde);
        
        ListaDePedidos = new HashSet<>();    
    }
    
    public void PreparaGUI(Controller c){
        telaLogin = new LoginJFrame(c);
        telaLogin.setVisible(true);
        telaMain = new mainJFrame(c);
        telaMain.setVisible(false);
    }
    
    public Set<Pedido> GetListaDePedidos(){
        return ListaDePedidos;
    
    }
    
    public Estoque GetEstoque(){
        return estoqueRestaurante;
    }
    
    public void setObservacaoPedido(Pedido p, String s){
        p.setObservacao(s);
    }
    
    public Pedido NovoPedido(String nome){    
        Pedido ped = new Pedido(nome);
        ListaDePedidos.add(ped);
        return ped;
    }
    
    public int checkLogin(String login, String senha){
        boolean loginvalido, senhavalida;
        
        loginvalido = false;
        senhavalida = false;
        
        String temp = " ";
        String tempsenha = " ";
        
        for(int x=0; !temp.equals("*EOF*"); x++){
            
            try{temp = bancoDeLogin.leitor(x);}
            catch(Exception e){}
            
            if(temp.equals(login)){
                loginvalido = true;
                
                try{tempsenha = bancoDeSenha.leitor(x);}
                catch(Exception e){}
                
                if(tempsenha.equals(senha)){
                    senhavalida = true;
                    break;
                }
                else{break;}
            }
        }
        if(loginvalido && senhavalida){
            telaLogin.setVisible(false);
            telaMain.setVisible(true);
            return 0;
        }
        else{
            if(loginvalido){
                return 1; // senha errada
            }
            else{
                return 2; //senha e/ou login errados
            }
            
        }
    }
    
   
    public static void main(String[] args){
        
        Controller controller = new Controller();
        controller.PreparaGUI(controller);
        
        
       
    } 
      
    }
    
    
