package Controller;
import Model.*;


import Model.Produto;
import java.util.Iterator;
import javax.swing.DefaultListModel;
    
public class Modelo {
    private DefaultListModel modeloListaBebidas;
    private DefaultListModel modeloListaPratos;
    private DefaultListModel modeloListaSobremesas;
    private Estoque estoque;
    
    public Modelo(Estoque estoque){
        this.estoque = estoque;
        modeloListaBebidas = new DefaultListModel();
        modeloListaPratos = new DefaultListModel();
        modeloListaSobremesas = new DefaultListModel();
        
        Iterator<Produto> it = estoque.getEstoque().iterator();
        while(it.hasNext()){
            Produto prod = it.next();
            switch (prod.getCategoria()) {
                case 1:
                    if(prod.getQuantidade()>=estoque.getLimite()){
                    modeloListaBebidas.addElement(prod.getNome());
                    }
                    
                    System.out.println(prod.getNome()+ "1");
                    break;
                case 2:
                    if(prod.getQuantidade()>=estoque.getLimite()){
                    modeloListaPratos.addElement(prod.getNome());
                    }
                    
                    System.out.println(prod.getNome()+ "2");
                    break;
                case 3:
                    if(prod.getQuantidade()>=estoque.getLimite()){
                    modeloListaSobremesas.addElement(prod.getNome());
                    }
                    
                    System.out.println(prod.getNome()+ "3");
                    break;
                default:
                    
                    break;
            }
        }
    }
    
    public DefaultListModel getModeloListaBebidas(){
        return modeloListaBebidas;
    }
    public DefaultListModel getModeloListaPratos(){
        return modeloListaPratos;
    }
    public DefaultListModel getModeloListaSobremesas(){
        return modeloListaSobremesas;
    }
    
}
