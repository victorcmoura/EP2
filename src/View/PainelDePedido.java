/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package View;
import Model.*;
import Controller.*;
import java.util.Iterator;
import javax.swing.DefaultListModel;
import javax.swing.table.DefaultTableModel;


public class PainelDePedido extends javax.swing.JPanel {

    private Pedido pedido;
    private Estoque estoque;
    private Modelo model;
    private Double preco;
    private String observacao;
    
    DefaultTableModel modeloTabelaPedido;
    
    
    public PainelDePedido(Pedido pedido, Estoque estoque, Modelo model) {
        initComponents();
        txtQuantia.setFocusable(false);
        txtQuantia.setEditable(false);
        preco = 0.0;
        this.model = model;
        this.estoque = estoque;
        observacao = "";
        pedido.setObservacao(observacao);
        
        modeloTabelaPedido = (DefaultTableModel)tabelaPedido.getModel();
       
        listaEstoqueBebidas.setModel(model.getModeloListaBebidas());
        listaEstoquePratos.setModel(model.getModeloListaPratos());
        listaEstoqueSobremesas.setModel(model.getModeloListaSobremesas());
        
        nomePedido_Painel.setText(pedido.getNome());
        precoPedido_Painel.setText(pedido.getPreco().toString());
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        nomePedido_Painel = new javax.swing.JLabel();
        precoPedido_Painel = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        listaEstoqueBebidas = new javax.swing.JList<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        listaEstoqueSobremesas = new javax.swing.JList<>();
        jScrollPane3 = new javax.swing.JScrollPane();
        listaEstoquePratos = new javax.swing.JList<>();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        botaoAddBebida = new javax.swing.JButton();
        botaoAddPrato = new javax.swing.JButton();
        botaoAddSobremesa = new javax.swing.JButton();
        jScrollPane4 = new javax.swing.JScrollPane();
        tabelaPedido = new javax.swing.JTable();
        botaoPagar = new javax.swing.JButton();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtObs = new javax.swing.JTextArea();
        botaoDinheiro = new javax.swing.JRadioButton();
        botaoCartao = new javax.swing.JRadioButton();
        botaoSalvarOBS = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtTroco = new javax.swing.JTextField();
        txtQuantia = new javax.swing.JTextField();

        setMaximumSize(new java.awt.Dimension(798, 629));
        setMinimumSize(new java.awt.Dimension(798, 629));
        setPreferredSize(new java.awt.Dimension(798, 629));

        nomePedido_Painel.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        nomePedido_Painel.setText("Pedido");

        precoPedido_Painel.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        precoPedido_Painel.setText("100,00");

        jLabel6.setFont(new java.awt.Font("Ubuntu", 0, 24)); // NOI18N
        jLabel6.setText("Total: R$");

        listaEstoqueBebidas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(listaEstoqueBebidas);

        listaEstoqueSobremesas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(listaEstoqueSobremesas);

        listaEstoquePratos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(listaEstoquePratos);

        jLabel1.setText("Bebidas:");

        jLabel2.setText("Pratos:");

        jLabel3.setText("Sobremesas:");

        botaoAddBebida.setText("Adicionar");
        botaoAddBebida.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoAddBebidaActionPerformed(evt);
            }
        });

        botaoAddPrato.setText("Adicionar");
        botaoAddPrato.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoAddPratoActionPerformed(evt);
            }
        });

        botaoAddSobremesa.setText("Adicionar");
        botaoAddSobremesa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoAddSobremesaActionPerformed(evt);
            }
        });

        tabelaPedido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Produto", "Preço Und."
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Double.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tabelaPedido.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        tabelaPedido.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        tabelaPedido.setFocusable(false);
        tabelaPedido.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tabelaPedido);

        botaoPagar.setText("Pagar");
        botaoPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoPagarActionPerformed(evt);
            }
        });

        txtObs.setColumns(20);
        txtObs.setRows(5);
        jScrollPane5.setViewportView(txtObs);

        buttonGroup1.add(botaoDinheiro);
        botaoDinheiro.setText("Dinheiro");
        botaoDinheiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoDinheiroActionPerformed(evt);
            }
        });

        buttonGroup1.add(botaoCartao);
        botaoCartao.setText("Cartão");
        botaoCartao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoCartaoActionPerformed(evt);
            }
        });

        botaoSalvarOBS.setText("Salvar");
        botaoSalvarOBS.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botaoSalvarOBSActionPerformed(evt);
            }
        });

        jLabel4.setText("Observação:");

        txtTroco.setEditable(false);
        txtTroco.setText("Troco");
        txtTroco.setFocusable(false);

        txtQuantia.setText("Quantia");
        txtQuantia.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtQuantiaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtQuantiaFocusLost(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(nomePedido_Painel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(precoPedido_Painel, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(108, 108, 108)
                                .addComponent(txtTroco, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1))
                                    .addGap(46, 46, 46)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel2)
                                        .addComponent(botaoAddPrato, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(botaoAddBebida, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane4)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 47, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel3)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                            .addComponent(botaoAddSobremesa, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                            .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(botaoSalvarOBS, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(botaoCartao)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(botaoDinheiro))
                                    .addComponent(txtQuantia)
                                    .addComponent(botaoPagar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(10, 10, 10)))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(nomePedido_Painel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addGap(7, 7, 7)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botaoAddBebida)
                    .addComponent(botaoAddPrato)
                    .addComponent(botaoAddSobremesa))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 2, Short.MAX_VALUE)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(botaoSalvarOBS)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(botaoCartao)
                            .addComponent(botaoDinheiro, javax.swing.GroupLayout.PREFERRED_SIZE, 24, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtQuantia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTroco, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(precoPedido_Painel, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(botaoPagar)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void botaoAddPratoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoAddPratoActionPerformed
        String nomePrato = listaEstoquePratos.getSelectedValue();
        System.out.println(nomePrato);
        Produto prod;
        
        
        Iterator<Produto> it = estoque.getEstoque().iterator();
        while(it.hasNext()){
            prod = it.next();
            if(prod.getNome().equals(nomePrato)){
                modeloTabelaPedido.addRow(new Object[]{prod.getNome(), prod.getPreco()});
                preco += prod.getPreco();
                precoPedido_Painel.setText(preco.toString());
                
                Integer limite = estoque.getLimite();
                try{limite = estoque.vendaProduto(prod.getId());}
                catch(Exception e){}
                if(limite.equals(1)){
                    model.getModeloListaPratos().remove(listaEstoquePratos.getSelectedIndex());
                }
            
            }
        }
        
        
    }//GEN-LAST:event_botaoAddPratoActionPerformed

    private void botaoAddBebidaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoAddBebidaActionPerformed
        String nomePrato = listaEstoqueBebidas.getSelectedValue();
        System.out.println(nomePrato);
        Produto prod;
        
        
        Iterator<Produto> it = estoque.getEstoque().iterator();
        while(it.hasNext()){
            prod = it.next();
            if(prod.getNome().equals(nomePrato)){
                modeloTabelaPedido.addRow(new Object[]{prod.getNome(), prod.getPreco()});
                preco += prod.getPreco();
                precoPedido_Painel.setText(preco.toString());
                
                Integer limite = estoque.getLimite();
                try{limite = estoque.vendaProduto(prod.getId());}
                catch(Exception e){}
                if(limite.equals(1)){
                    model.getModeloListaBebidas().remove(listaEstoqueBebidas.getSelectedIndex());
                }
            
            }
        }
    }//GEN-LAST:event_botaoAddBebidaActionPerformed

    private void botaoAddSobremesaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoAddSobremesaActionPerformed
        String nomePrato = listaEstoqueSobremesas.getSelectedValue();
        System.out.println(nomePrato);
        Produto prod;
        
        
        Iterator<Produto> it = estoque.getEstoque().iterator();
        while(it.hasNext()){
            prod = it.next();
            if(prod.getNome().equals(nomePrato)){
                modeloTabelaPedido.addRow(new Object[]{prod.getNome(), prod.getPreco()});
                preco += prod.getPreco();
                precoPedido_Painel.setText(preco.toString());
                
                Integer limite = estoque.getLimite();
                try{limite = estoque.vendaProduto(prod.getId());}
                catch(Exception e){}
                if(limite.equals(1)){
                    model.getModeloListaSobremesas().remove(listaEstoqueSobremesas.getSelectedIndex());
                }
            
            }
        }
    }//GEN-LAST:event_botaoAddSobremesaActionPerformed

    private void botaoPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoPagarActionPerformed
        
        if(txtQuantia.isFocusable()&&botaoDinheiro.isSelected()){
            try{Double quantiaDouble = new Double(txtQuantia.getText());
            if(quantiaDouble-preco>=0){
                txtTroco.setText("Troco: R$"+(quantiaDouble-preco));
            }
            else{
                new Aviso().setTexto("Quantia inválida!");
            }
            
        }
        catch(Exception e){new Aviso().setTexto("Quantia inválida!");}
        }
        else if(!txtQuantia.isFocusable()&&botaoCartao.isSelected()){
            new Aviso().setTexto("Pagar na maquina");
            botaoAddBebida.setEnabled(false);
            botaoAddPrato.setEnabled(false);
            botaoAddSobremesa.setEnabled(false);
        }
        else{
            new Aviso().setTexto("Selecione método");
        }
        
        
        
        
        
        
       
    }//GEN-LAST:event_botaoPagarActionPerformed

    private void botaoDinheiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoDinheiroActionPerformed
        txtQuantia.setFocusable(true);
        txtQuantia.setEditable(true);
        txtQuantia.setText("Quantia");
    }//GEN-LAST:event_botaoDinheiroActionPerformed

    private void botaoCartaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoCartaoActionPerformed
        txtQuantia.setFocusable(false);
        txtQuantia.setEditable(false);
        txtQuantia.setText("Quantia");
        txtTroco.setText("Troco: ");
    }//GEN-LAST:event_botaoCartaoActionPerformed

    private void txtQuantiaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtQuantiaFocusGained
        txtQuantia.setText("");
    }//GEN-LAST:event_txtQuantiaFocusGained

    private void txtQuantiaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtQuantiaFocusLost
        if(txtQuantia.getText().equals("")){
            txtQuantia.setText("Quantia");
        }
    }//GEN-LAST:event_txtQuantiaFocusLost

    private void botaoSalvarOBSActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botaoSalvarOBSActionPerformed
        observacao = txtObs.getText();
    }//GEN-LAST:event_botaoSalvarOBSActionPerformed
    
    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botaoAddBebida;
    private javax.swing.JButton botaoAddPrato;
    private javax.swing.JButton botaoAddSobremesa;
    private javax.swing.JRadioButton botaoCartao;
    private javax.swing.JRadioButton botaoDinheiro;
    private javax.swing.JButton botaoPagar;
    private javax.swing.JButton botaoSalvarOBS;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JList<String> listaEstoqueBebidas;
    private javax.swing.JList<String> listaEstoquePratos;
    private javax.swing.JList<String> listaEstoqueSobremesas;
    private javax.swing.JLabel nomePedido_Painel;
    private javax.swing.JLabel precoPedido_Painel;
    private javax.swing.JTable tabelaPedido;
    private javax.swing.JTextArea txtObs;
    private javax.swing.JTextField txtQuantia;
    private javax.swing.JTextField txtTroco;
    // End of variables declaration//GEN-END:variables
}
