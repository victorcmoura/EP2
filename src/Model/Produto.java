package Model;


public class Produto {
	private double preco;
	private String nome;
	private int id;
	private int categoria;
	private int quantidade;
	
	public Produto(){
		///ABRIR CAMPOS DE TEXTO E SALVAR INFORMAÇÕES NAS VARIAVEIS;
		this.preco = 10;
		this.nome = "Generico";
		this.id = 1001;
		this.categoria = 1;
		this.quantidade = 10;
	}
	
	public Produto(String nome, double preco, int id, int categoria, int quantidade){
		this.nome = nome;
		this.categoria = categoria;
		this.id = id;
		this.preco = preco;
		this.quantidade = quantidade;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public int getId() {
		return id;
	}

	public int getCategoria() {
		return categoria;
	}
	
}
