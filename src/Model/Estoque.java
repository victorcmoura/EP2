package Model;
import Controller.*;
import View.Aviso;


import java.util.*;

public class Estoque {
	private Set<Produto> listaEstoque;
        private Integer limite;
        
        
	
	public Estoque(Arquivo nome, Arquivo preco, Arquivo id, Arquivo categoria, Arquivo qtde){
		limite = 0;
                listaEstoque = new HashSet<>();
                String temp = " ";
                int x = 1;
                
                do{
                    
                    try{
                            temp = nome.leitor(x);
                }
                
                    catch(Exception e){}
                    
                    
                    
                    Produto p = null;
                    //try{System.out.println(nome.leitor(x));}
                    //catch(Exception e){}
                    try{p = new Produto(nome.leitor(x), Double.parseDouble(preco.leitor(x)), Integer.parseInt(id.leitor(x)), Integer.parseInt(categoria.leitor(x)), Integer.parseInt(qtde.leitor(x)));}
                    catch(Exception e){}
                    listaEstoque.add(p);
                    x++;
                    
                    try{
                            temp = nome.leitor(x);
                }
                
                    catch(Exception e){}
                    
                    
                }while(!temp.equals("*EOF*"));
                
                Iterator<Produto> it = listaEstoque.iterator();
		while(it.hasNext()){
			Produto prod = it.next();
			                 System.out.println("Nome: "+prod.getNome());
                                         System.out.println("Preco: "+prod.getPreco());
                                         System.out.println("Id: "+prod.getId());
                                         System.out.println("Categoria: "+prod.getCategoria());
                                         System.out.println("Quantidade: "+prod.getQuantidade());
                                         System.out.println("************");
                                         
			
		}
                
        }
        
        public Integer getLimite(){
            return limite;
        }
        
        public void setLimite(Integer limite){
            this.limite = limite;
        }
	
	public void AddProdutoEstoque(String nome, double preco, int id, int categoria, int quantidade){
		Produto p;
                p = new Produto(nome, preco, id, categoria, quantidade);
		this.listaEstoque.add(p);
                
	}
	
	public void ExcluirProdutoEstoque(int id) throws Exception{
		boolean fail = true;
		Iterator<Produto> it = listaEstoque.iterator();
		while(it.hasNext()){
			Produto prod = it.next();
			if(id == prod.getId()){
				it.remove();
				fail = false;
			}
			
		}
		if(fail){
			throw new Exception("Produto não encontrado");
		}
	}
	
	public int vendaProduto(int id) throws Exception{
		Iterator<Produto> it = listaEstoque.iterator();
		boolean fail = true;
                int qtdeAlert = 0;
		while(it.hasNext()){
                        Produto prod = it.next();
			if(id == prod.getId()){
				prod.setQuantidade(prod.getQuantidade()-1);
				fail = false;
				if(prod.getQuantidade() <= limite){
					new Aviso();
                                       qtdeAlert = 1;
				}
                                
			}
		}
		if(fail){
			throw new Exception("Produto não encontrado");
		}
                else{return qtdeAlert;}
                	
	}
	public void retornoProduto(int id) throws Exception{
		Iterator<Produto> it = listaEstoque.iterator();
		boolean fail = true;
		while(it.hasNext()){
			if(id == it.next().getId()){
				it.next().setQuantidade(it.next().getQuantidade()+1);
				fail = false;
			}
		}
		if(fail){
			throw new Exception("Produto não encontrado");
		}
		
	}
	
	public Set<Produto> getEstoque(){
		return this.listaEstoque;
	}
		
	
	
	
}
