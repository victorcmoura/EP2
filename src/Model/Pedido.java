package Model;

import java.text.DecimalFormat;
import java.util.*;


public class Pedido {
        private String Nome;
	private int quantidade;
	private Double preco;
	private String observacao;
	private List<Produto> produtos;
	
	public Pedido(String nome){
                this.Nome = nome;
		this.quantidade = 0;
		this.preco = 0.00;
		produtos = new ArrayList<>();
                observacao = "";
	}
	public void addProduto(){
		Produto p = new Produto();
		produtos.add(p);
		this.preco = getPreco() + p.getPreco();
                this.quantidade++;
	}
        public void addProduto(Produto p){
		
		produtos.add(p);
		this.preco += p.getPreco();
                this.quantidade++;
	}
	public void removerProduto(int id) throws Exception{
		Iterator<Produto> it = produtos.iterator();
		boolean fail = true;
		while(it.hasNext()){
			Produto prod = it.next();
			if(id == prod.getId()){
				fail = false;
				this.preco = getPreco() - prod.getPreco();
                                it.remove();
                                this.quantidade--;
                                
			}
		}
		if(fail){
			throw new Exception("Produto não encontrado");
		}
	}
	public void fecharCompra(){
		
	}
	public int getQuantidade() {
		return quantidade;
	}
	public Double getPreco() {
		return preco;
	}
        public String getNome(){
            return Nome;
        }
        public String getObservacao(){
            return observacao;
        }
        public void setObservacao(String s){
            this.observacao = s;
        }
        
}
