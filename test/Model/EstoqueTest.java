/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Model;

import Controller.Arquivo;
import java.util.HashSet;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author victor
 */
public class EstoqueTest {
    Estoque instance;
    
    public EstoqueTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
            Arquivo nome = new Arquivo("test.bin");
            Arquivo preco = new Arquivo("test.bin");
            Arquivo id = new Arquivo("test.bin");
            Arquivo categoria = new Arquivo("test.bin");
            Arquivo qtde = new Arquivo("test.bin");
            this.instance = new Estoque(nome, preco, id, categoria, qtde);
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getLimite method, of class Estoque.
     */
    @Test
    public void testGetLimite() {
        System.out.println("getLimite");
        
        Integer expResult = 0;
        Integer result = instance.getLimite();
        assertEquals(expResult, result);
        
        //fail("Result: "+result);
    }

    /**
     * Test of setLimite method, of class Estoque.
     */
    @Test
    public void testSetLimite() {
            
        System.out.println("setLimite");
        Integer limite = 0;
        
        instance.setLimite(limite);
        
        assertEquals(limite, instance.getLimite());
        
        
        //fail("Limite: 0, Result: "+instance.getLimite());
        
    }

    /**
     * Test of AddProdutoEstoque method, of class Estoque.
     */
    @Test
    public void testAddProdutoEstoque() {
        System.out.println("AddProdutoEstoque");
        String nome = "";
        double preco = 0.0;
        int id = 0;
        int categoria = 0;
        int quantidade = 0;
        Produto expResult = new Produto(nome, preco, id, categoria, quantidade);
        instance.AddProdutoEstoque(nome, preco, id, categoria, quantidade);
        
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of ExcluirProdutoEstoque method, of class Estoque.
     */
    @Test
    public void testExcluirProdutoEstoque() throws Exception {
        System.out.println("ExcluirProdutoEstoque");
        String nome = "";
        double preco = 0.0;
        int id = 0;
        int categoria = 0;
        int quantidade = 0;
        instance.AddProdutoEstoque(nome, preco, id, categoria, quantidade);
        instance.ExcluirProdutoEstoque(id);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of vendaProduto method, of class Estoque.
     */
    @Test
    public void testVendaProduto() throws Exception {
        System.out.println("vendaProduto");
        
        int expResult = 1;
        String nome = "";
        double preco = 0.0;
        int id = 0;
        int categoria = 0;
        int quantidade = 0;
        instance.AddProdutoEstoque(nome, preco, id, categoria, quantidade);
        int result = instance.vendaProduto(id);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of retornoProduto method, of class Estoque.
     * @throws java.lang.Exception
     */
    @Test
    public void testRetornoProduto() throws Exception {
        System.out.println("retornoProduto");
        
        String nome = "";
        double preco = 0.0;
        int id = 1;
        int categoria = 0;
        int quantidade = 0;
        instance.AddProdutoEstoque(nome, preco, id, categoria, quantidade);
        
        instance.retornoProduto(id);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of getEstoque method, of class Estoque.
     */
    @Test
    public void testGetEstoque() {
        System.out.println("getEstoque");
        String nome = "";
        double preco = 0.0;
        int id = 0;
        int categoria = 0;
        int quantidade = 0;
        instance.AddProdutoEstoque(nome, preco, id, categoria, quantidade);
        
        Produto novo = new Produto(nome, preco, id, categoria, quantidade);
                
        Set<Produto> expResult = new HashSet<>();
        expResult.add(novo);
        expResult.add(new Produto(nome, 100, id, categoria, quantidade));
        expResult.add(new Produto("Test", preco, id, categoria, quantidade));
        expResult.add(new Produto(nome, preco, 10, categoria, quantidade));
        Set<Produto> result = instance.getEstoque();
        
        
        
        assertEquals(expResult.size(), result.size());
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
}
